<?php get_header(); ?>

<section class="page-banner" style="background-image: url(<?php echo get_theme_file_uri('/images/mainm.jpeg')?>);" id="home">
      <div class="page-banner__overlay">
        <div class="container page-banner--content">
        <h1 class="page-banner__title" data-aos="zoom-out" data-aos-duration="3000">
          BUDMONT <span style="color: #e96b39">PLUS</span>
        </h1>
        <div class="page-banner__text">
          <h2 class="page-banner__subtitle heading--medium" data-aos="zoom-out" data-aos-duration="3000">
          <?php the_field('subtitle'); ?>
          </h2>         
        </div>
        <div class="page-banner--buttons">
          <a href="#contact" class="button--place" data-aos="fade-up" data-aos-duration="3000">
              <button class="btn btn--white main-btn">
                <?php the_field('main_button'); ?>
              </button>
            </a>
            <a href="#realizations" data-aos="fade-up" data-aos-duration="3000">
              <button class="btn btn--white-all main-btn">
                Zobacz Realizacje
              </button>
            </a>
        </div>
        </div>
        </div>
  </div>
</section>

<div class="realizations" id="realizations">
  <div class="container">
    <h1 class="realizations__title heading" data-aos="fade-up" data-aos-duration="3000">
      <?php the_field('realization_title'); ?>
    </h1>
      <p class="realizations__description" data-aos="fade-up" data-aos-duration="3000">
        <?php the_field('realization_subtitle'); ?>
      </p>
      <div class="hero-slider">
          <div class="glide" id="glide1">
          <div class="glide__track" data-glide-el="track">
            <div class="glide__slides">
              <div class="hero-slider__slide" style="background-image: url(<?php the_field('realization_image_1'); ?>);">
              </div>
        
              <div class="hero-slider__slide" style="background-image: url(<?php the_field('realization_image_2'); ?>);">
              </div>
      
              <div class="hero-slider__slide" style="background-image: url(<?php the_field('realization_image_3'); ?>);">
              </div>
              
              <div class="hero-slider__slide" style="background-image: url(<?php the_field('realization_image_4'); ?>);">
              </div>
              
              <div class="hero-slider__slide" style="background-image: url(<?php the_field('realization_image_5'); ?>);">
              </div>

            </div>
          </div>
          <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><i class="fa fa-arrow-left"></i></button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><i class="fa fa-arrow-right"></i></button>
          </div>
        </div>
        </div>
          <span class="realizations__description" data-aos="fade-up" data-aos-duration="3000"><?php the_field('realization_description'); ?></span>
          <a href="<?php echo site_url('/realizacje')?>"><button class="btn btn--black hero-slider__button" data-aos="zoom-out" data-aos-duration="3000">
            <?php the_field('realization_button'); ?>
          </button></a>
  </div>
</div>

<div class="offer">
    <div class="container offer__content">

    <div class="offer__col-one">
      <h1 class="offer__title heading" data-aos="fade-up" data-aos-duration="3000">
        <?php the_field('offer_title'); ?>
      </h1>
      <img src="<?php the_field('offer_image'); ?>" class="offer__image" data-aos="zoom-out" data-aos-duration="3000">
      <p class="offer__description" data-aos="fade-up" data-aos-duration="3000">
        <?php the_field('offer_description'); ?>
      </p>
      <a href="<?php echo site_url('/oferta')?>"><button class="btn btn--black" data-aos="zoom-out" data-aos-duration="3000"><?php the_field('offer_button'); ?></button></a>
    </div>
    <div class="offer__col-two">
      <h1 class="offer__title heading" data-aos="fade-up" data-aos-duration="3000">
        <?php the_field('about_us_title'); ?>
      </h1>
      <img src=" <?php the_field('about_us_image'); ?>" class="offer__image" data-aos="zoom-out" data-aos-duration="3000">
      <p class="offer__description" data-aos="fade-up" data-aos-duration="3000">
        <?php the_field('about_us_description'); ?>
      </p>
      <a href="<?php echo site_url('/o-firmie')?>"><button class="btn btn--black" data-aos="zoom-out" data-aos-duration="3000"><?php the_field('about_us_button'); ?></button></a>
    </div>
    </div>
  </div>

  <?php
    get_template_part('modules/why-us');
  ?>

  <?php
    get_template_part('modules/partners');
  ?>

<section class="contact-section">
  <div class="container container--narrow page-section t-center">
    <div class="contact__content">
      <h3 class="contact__content--title heading--medium" data-aos="fade-up" data-aos-duration="3000">
       Zapraszam do kontaktu
      </h3>   
      <p class="contact__content--description" data-aos="fade-up" data-aos-duration="3000">
        Każde zlecenie wyceniamy indywidualnie, w zależności od zakresu prac.<br>
        Zainteresowanych naszymi usługami bądź wyceną zlecenia zapraszamy do kontaktu telefonicznego lub mailowo!<br>   
        Na każdą wiadomość postaramy się odpowiedzieć tak szybko jak to możliwe!     
      </p>
      <a href="mailto:budmontplus@gmail.com">   
        <button class="btn btn--black" data-aos="zoom-out" data-aos-duration="3000">
           Napisz do mnie
        </button>
      </a>        
    </div>
  </div>
</section>
 

<?php get_footer();

?>
