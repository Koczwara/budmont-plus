<?php get_header(); ?>

<div class="about-us__page">
  <div class="container">
    <h1 class="about-us__page--title heading" data-aos="fade-up" data-aos-duration="3000">
      <?php the_field('title'); ?>
    </h1>
    <h3 class="about-us__page--subtitle heading--medium" data-aos="fade-up" data-aos-duration="3000">
      <?php the_field('subtitle_1'); ?>
    </h3>
    <p class="about-us__page--description" data-aos="fade-up" data-aos-duration="3000">
       <?php the_field('description_1'); ?>
    </p>
    <h3 class="about-us__page--subtitle heading--medium" data-aos="fade-up" data-aos-duration="3000">
     <?php the_field('subtitle_2'); ?>
    </h3>
    <p class="about-us__page--description" data-aos="fade-up" data-aos-duration="3000">
     <?php the_field('description_2'); ?>
    </p>
    <h3 class="about-us__page--subtitle heading--medium" data-aos="fade-up" data-aos-duration="3000">
      <?php the_field('subtitle_3'); ?>
    </h3>
    <p class="about-us__page--description" data-aos="fade-up" data-aos-duration="3000">
     <?php the_field('description_3'); ?>
    </p>
    <h3 class="about-us__page--subtitle heading--medium" data-aos="fade-up" data-aos-duration="3000">
     <?php the_field('subtitle_4'); ?>
    </h3>
    <p class="about-us__page--description" data-aos="fade-up" data-aos-duration="3000">
     <?php the_field('description_4'); ?>
    </p>
  </div>
</div>

<?php get_template_part('modules/why-us'); ?>


<?php get_footer();?>