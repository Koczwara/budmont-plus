<?php get_header(); ?>

<div class="price-list__page" data-aos="fade-up" data-aos-duration="3000">
  <div class="container">
    <h1 class="price-list__page--title heading">
      <?php the_field('price_list_title'); ?>
    </h1>
    <ul>
      <li><?php the_field('list_item_1'); ?></li>
      <li><?php the_field('list_item_2'); ?></li>
      <li><?php the_field('list_item_3'); ?></li>
      <li><?php the_field('list_item_4'); ?></li>
    </ul>
  </div>
</div>
<div class="price-list__page--pricing">
  <div class="container price-list__page--pricing--content">
  <div class="price-list__page--pricing__text">
    <h2 class="price-list__page--pricing__text--subtitle heading--medium" data-aos="fade-up" data-aos-duration="3000"> Rozpocznijmy współpracę już dziś!</h2>
    <p class="price-list__page--pricing__text--description" data-aos="fade-up" data-aos-duration="3000">Zadzwoń lub napisz i zapytaj o bezpłatną, szczegółową wycenę.</p>
  </div>
  <a href="#contact"><button class="btn btn--black">Darmowa wycena</button data-aos="fade-up" data-aos-duration="3000"></a>
  </div>
</div>


<?php get_footer();?>