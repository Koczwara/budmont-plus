<?php

function budmont_files() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap', '//fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  
  if (strstr($_SERVER['SERVER_NAME'], 'budmont-plus.local')) {
    wp_enqueue_script('main-budmont-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
  } else {
    wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.8bbd7a714a7d678aaff5.js'), NULL, '1.0', true);
    wp_enqueue_script('main-budmont-js', get_theme_file_uri('/bundled-assets/scripts.e91bbcbbc4057356ed42.js'), NULL, '1.0', true);
    wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.e91bbcbbc4057356ed42.css'));
    wp_enqueue_style('our-vendors-styles', get_theme_file_uri('/bundled-assets/styles.e91bbcbbc4057356ed42.css'));
  }
  wp_localize_script('main-budmont-js', 'budmontData', array(
    'root_url' => get_site_url()
  ));
  
}

add_action('wp_enqueue_scripts', 'budmont_files');

function budmont_features() {
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'budmont_features');

