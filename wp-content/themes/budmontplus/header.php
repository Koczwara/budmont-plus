<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  
  <header class="site-header">
    <div class="container">
      <a href="<?php echo site_url() ?>" class="logo"><span class="plus">+ </span>Budmont Plus<span></a>
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="nav main-navigation">
          <ul>
            <li <?php if(is_front_page()) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url()?>">STRONA GŁÓWNA</a></li>
            <li <?php if(is_page('o-firmie')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/o-firmie')?>">O FIRMIE</a></li>
            <li <?php if(is_page('oferta')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/oferta')?>">OFERTA</a></li>
            <li <?php if(is_page('cennik')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/cennik')?>">CENNIK</a></li>
            <li <?php if(is_page('realizacje')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/realizacje')?>">REALIZACJE</a></li>
            <li <?php if(is_page('oferta')) echo 'class="current-menu-item"' ?>><a href="#contact">KONTAKT</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>




 