<?php get_header(); ?>

<div class="realizations-page">
  <div class="container">
    <h1 class="realizations-page--title heading" data-aos="fade-up" data-aos-duration="3000">
      Poznaj nasze realizacje
    </h1>
  </div>
</div>

<div class="container container--narrow page-section"> 
    <section id="gallery">
        <div class="container">
            <div id="image-gallery" class="image-gallery">
                <div class="image">
                <div class="img-wrapper">
                <a href="<?php the_field('gallery_image_1'); ?>">
                    <img src="<?php the_field('gallery_image_1'); ?>" class="img-responsive">
                </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_2'); ?>">
                        <img src="<?php the_field('gallery_image_2'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_3'); ?>">
                        <img src="<?php the_field('gallery_image_3'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_4'); ?>">
                        <img src="<?php the_field('gallery_image_4'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_5'); ?>">
                        <img src="<?php the_field('gallery_image_5'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_6'); ?>">
                        <img src="<?php the_field('gallery_image_6'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_7'); ?>">
                        <img src="<?php the_field('gallery_image_7'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_8'); ?>">
                        <img src="<?php the_field('gallery_image_8'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_9'); ?>">
                        <img src="<?php the_field('gallery_image_9'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_10'); ?>">
                        <img src="<?php the_field('gallery_image_10'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_11'); ?>">
                        <img src="<?php the_field('gallery_image_11'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_12'); ?>">
                        <img src="<?php the_field('gallery_image_12'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_13'); ?>">
                        <img src="<?php the_field('gallery_image_13'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_14'); ?>">
                        <img src="<?php the_field('gallery_image_14'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_15'); ?>">
                        <img src="<?php the_field('gallery_image_15'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_16'); ?>">
                        <img src="<?php the_field('gallery_image_16'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_17'); ?>">
                        <img src="<?php the_field('gallery_image_17'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_18'); ?>">
                        <img src="<?php the_field('gallery_image_18'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>

                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_19'); ?>">
                        <img src="<?php the_field('gallery_image_19'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_20'); ?>">
                        <img src="<?php the_field('gallery_image_20'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?php the_field('gallery_image_21'); ?>">
                        <img src="<?php the_field('gallery_image_21'); ?>" class="img-responsive">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    
</div>

<?php get_footer();?>