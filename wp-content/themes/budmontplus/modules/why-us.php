<div class="why-us">
    <div class="container">
    <div class="why-us__col-one">
    <h1 class="why-us__title heading" data-aos="fade-up" data-aos-duration="3000">
      Dlaczego My?
    </h1>
    <ul data-aos="fade-up" data-aos-duration="3000">
      <li>Zgrany zespół specjalistów</li>
      <li>Kompleksowe wsparcie</li>
      <li>Sprawdzone rozwiązania i materiały</li>
      <li>Najwyższa jakość wykonywanych usług</li>
      <li>Dostosowanie produktów do specyfiki budynku </li>
      <li>80% naszych nowych klientów, to klienci z polecenia</li>
      <li>Gwarancja satysfakcjonujących efektów pracy</li>
    </ul>
    </div>
    <div class="why-us__col-two">
      <img src="<?php echo get_theme_file_uri('/images/brick.png')?>" class="why-us__image" data-aos="zoom-out" data-aos-duration="3000">
    </div>
    </div>
</div>