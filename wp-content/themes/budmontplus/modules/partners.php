<div class="partners">
    <div class="container">
      <h1 class="partners__title heading" data-aos="fade-up" data-aos-duration="3000">
        Partnerzy firmy
      </h1>
      <div class="hero-slider">
          <div class="glide" id="glide2">
            <div class="glide__track" data-glide-el="track">
              <div class="glide__slides_2 partners__logos">
                    <div class="hero-slider__slide">
                      <img src="<?php echo get_theme_file_uri('/images/logo1.png')?>" class="logo">
                    </div>
              
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo2.png')?>" class="logo">
                    </div>
            
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo3.png')?>" class="logo">
                    </div>
                    
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo4.png')?>" class="logo">
                    </div>
                    
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo5.png')?>" class="logo">
                    </div>

                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo6.png')?>" class="logo">
                    </div>
                    
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo7.png')?>" class="logo">
                    </div>

                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo8.png')?>" class="logo">
                    </div>
                    
                    <div class="hero-slider__slide">
                    <img src="<?php echo get_theme_file_uri('/images/logo9.png')?>" class="logo">
                    </div>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>