<footer class="site-footer" id="contact">
      <div class="site-footer__inner container">
        <div class="group">

          <div class="site-footer__col-one">        
            <a href="<?php echo site_url() ?>" class="logo--footer"><span class="plus">+ </span>Budmont Plus<span></a>
          </div>

          <div class="site-footer__col-two">
            <h6 class="site-footer--title">Siedziba</h6>
            <p class="site-footer__link">Borów 154<br>23-235 Annopol</p>
          </div>
  
          <div class="site-footer__col-three">
            <h6 class="site-footer--title">Kontakt</h6>
            <p><a class="site-footer__link" href="tel:512 683 144"><i class="fa fa-phone" aria-hidden="true"></i> Tel.kom: 512 683 144</a></p>
            <p><a class="site-footer__link" href="mailto:budmontplus@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: budmontplus@gmail.com</a></p>
          </div>

          <div class="site-footer__col-four">
            <h6 class="site-footer--title">Mapa Strony</h6>
              <nav class="footer-navigation">
                <ul>
                  <li><a href="<?php echo site_url()?>">STRONA GŁÓWNA</a></li>
                  <li><a href="<?php echo site_url('/o-firmie')?>">O FIRMIE</a></li>
                  <li><a href="#<?php echo site_url('/oferta')?>">OFERTA</a></li>
                  <li><a href="<?php echo site_url('/cennik')?>">CENNIK</a></li>
                  <li><a href="<?php echo site_url('/realizacje')?>">REALIZACJE</a></li>
                </ul>
              </nav>
          </div>

        </div>
      </div>


      <div class="site-footer__under__inner container">
        <div class="site-footer__under__inner__col-one">        
            <p class="privacy-policy">polityka prywatności</p>
        </div>
        <div class="site-footer__under__inner__col-two social-icons-list">      
          <a href="https://www.facebook.com/Budmont-Plus-101184924722638" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </div>
        <div class="site-footer__under__inner__col-three">        
            <p>© 2020 Budmont Plus | by <a href="http://patrycjakoczwara.pl/" target="_blank">Patrycja Koczwara</a></p>
        </div>
      </div>

    </footer>
<?php wp_footer(); ?>
</body>
</html>