<?php get_header(); ?>
<div class="offer-page" data-aos="fade-up" data-aos-duration="3000">
  <div class="container">
    <h1 class="offer-page--title heading">
      <?php the_field('title'); ?>
    </h1>
    <p class="offer-page--description">
      <?php the_field('description'); ?>
    </p>
    <h2 class="offer-page--subtitle heading--medium">
      <?php the_field('subtitle_1'); ?>
    </h2>
    <p class="offer-page--description">
     <?php the_field('description_1'); ?>
    </p>
    <ul>
      <li><?php the_field('item_1'); ?></li>
      <li><?php the_field('item_2'); ?></li>
      <li><?php the_field('item_3'); ?></li>
      <li><?php the_field('item_4'); ?></li>
      <li><?php the_field('item_5'); ?></li>
      <li><?php the_field('item_6'); ?></li>
    </ul>
    <h2 class="offer-page--heading heading--medium">
      <?php the_field('subtitle_2'); ?>
    </h2>
    <p><?php the_field('description_2'); ?></p>
    <ul>
      <li><?php the_field('item_7'); ?></li>
      <li><?php the_field('item_8'); ?></li>
    </ul>
    <p class="offer-page--heading--description">
      <?php the_field('description_3'); ?>
    </p>

  </div>
</div>

<?php get_template_part('modules/why-us'); ?>


<section class="contact-section">
  <div class="container container--narrow page-section t-center">
    <div class="contact__content">
      <h3 class="contact__content--title heading--medium">
       Zapraszam do kontaktu
      </h3>   
      <p class="contact__content--description">
        Każde zlecenie wyceniamy indywidualnie, w zależności od zakresu prac.<br>
        Zainteresowanych naszymi usługami bądź wyceną zlecenia zapraszamy do kontaktu telefonicznego lub mailowo!<br>   
        Na każdą wiadomość postaramy się odpowiedzieć tak szybko jak to możliwe!     
      </p>
      <a href="mailto:budmontplus@gmail.com">   
        <button class="btn btn--black">
           Napisz do mnie
        </button>
      </a>        
    </div>
  </div>
</section>

<?php get_footer();?>